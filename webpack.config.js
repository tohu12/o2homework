var path = require('path');

module.exports = {
    devtool: 'eval',
    entry: [
        path.resolve(__dirname, './app/index.js')
    ],
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
        ]
    }
};