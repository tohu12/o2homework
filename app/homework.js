var $ = require("jquery");

/**
 * Main class for O2 homework
 */
export default class HomeWork {

    constructor($ele, $map) {
        this.$ele = $ele;
        this.$map = $map;
        this.gmap;
        this.geocoder;
        this.isApiLoaded = false;
        this.markers = [];

        this.bindEvents();
    }

    bindEvents() {
        this.$ele.on('newcontent', (e, $form) => {

            const search = $form.find('input').val();

            if (search) {
                if (!this.isApiLoaded) {
                    this._loadMap().done(() => {
                        this.isApiLoaded = true;
                        this.geoSearch(search);
                    });
                } else {
                    this.geoSearch(search);
                }
            } else {
                this._clearMap();
                this._triggerFoundEvent();
            }
        });
    }

    createMap() {
        this.gmap = new google.maps.Map(this.$map.get(0), {
            center: {
                lat: 50.039,
                lng: 15.768
            },
            zoom: 12
        });
    }

    geoSearch(search) {

        this.geocoder || (this.geocoder = new google.maps.Geocoder());

        this._clearMap();

        let bounds = new google.maps.LatLngBounds();

        this.geocoder.geocode({ 'address': search }, (res, stat) => {

            if (google.maps.GeocoderStatus.OK !== stat) {
                this._triggerFoundEvent();
                return;
            }

            res.forEach((addr, idx) => {

                if (idx > 9) return;

                this.gmap || this.createMap();

                addr.geometry.bounds && bounds.union(addr.geometry.bounds);

                let marker = new google.maps.Marker({
                    map: this.gmap,
                    position: addr.geometry.location
                });

                this.markers.push(marker);
            });

            this.gmap.fitBounds(bounds);

            this._triggerFoundEvent();
        });
    }

    _loadMap(search) {
        return $.getScript('//maps.google.com/maps/api/js?sensor=false');
    }

    _clearMap() {
        this.markers.forEach(m => m.setMap(null));
        this.markers = [];
    }

    _triggerFoundEvent() {
        $(this).trigger('found', [this.markers.length]);
    }
}
