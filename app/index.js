"use strict";

require("../css/style.css");
import $ from 'jquery'
import HomeWork from './homework'

const $doc = $(document),
    $form = $('form.map-search'),
    o2hw = new HomeWork($doc, $('#map'));

let timeout;

$form.find('input').on('keydown', () => {
    timeout && clearTimeout(timeout);
    timeout = setTimeout(() => {
        $doc.trigger('newcontent', [$form]);
    }, 300);
});

$(o2hw).on('found', (e, count) => {
    $form.find('label[for=address]').html(count);
});
