# O2 domácí úkol

## Zadání:

Vytvořit skript, který:

* sleduje jQuery custom event "newcontent"

* pokud nastane tato událost, vezme její cíl jako nový obsah a hledá v něm formulář s třídou "map-search" - pokud ho najde, načte API Google map (v nejširším smyslu slova, povolené jsou i knihovny)

* formulářový input (předpokládejme, že ve form.map-search je obsažený jen jediný) bude používat jako vstup pro geolokaci; jakmile bude mít nějaký bod zobrazitelný na mapě, vytvoří mapu hned pod formulář

* pak zobrazuje na mapě značky pro prvních deset výsledků geolokace, a to tak, aby všechny byly vidět

* API pro geolokaci provolává nejčastěji jednou za deset vteřin ( trochu umělý požadavek, lze ze zadání vypustit ) Ideálně, kdyby se co nejvíce kódu stahovalo až v okamžiku, kdy je opravdu potřeba.

## Instalace:

```
npm install webpack webpack-dev-server -g
npm install
npm start

http://localhost:8080
```

## Předpoklady:

Co musí být na počítači nainstalované:

* [nodejs](https://nodejs.org/) v aktuákní verzi

## Popis:

[Demo JSFiddle](https://jsfiddle.net/tohu/dz08swte/) - živá funkční ukázka.

Javascriptový kód je zapsán v syntaxi ES2015 s využitím [Babel](https://babeljs.io/) transpileru pro převod do ES5. Dále je použit [Webpack](https://webpack.github.io/) a [Webpack dev server](http://webpack.github.io/docs/webpack-dev-server.html) pro práci s moduly a běhu v lokálním prohlížeči na portu 8080.

Po spuštění stačí začít psát nějakou adresu do textového pole (např.: "masa") a pokud uživatel přestane na 300ms psát, tak se adresa automaticky vyhledá a zobrazí v mapě. Vedle zadané adresy se zobrazí počet nalezených adres.

## Nedostatky:

* Při zadání adresy např. "Masarykovo" Google API vraci pouze jednu adresu a ne všechny existující Masarykova náměstí.
* ... a našli by se další, třeba přidat unit testy :)
